import axios from 'axios'

export async function isLoggedIn () {
  let result = null
  await axios.get('/')
    .then(resp => {
      // return true if logged in (so does not include <form>)
      result = !resp.data.includes('<form action="/logon"')
    })
    .catch(error => {
      console.log(error)
      result = false
    })
  return result
}
