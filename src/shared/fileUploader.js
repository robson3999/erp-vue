/*
  This component is based on https://github.com/dhhb/vue-base64-file-upload
  extended to deal with multiple images
  RG
*/

if (!window.FileReader) {
  console.error('Your browser does not support FileReader API!')
}

export default {
  name: 'vue-base64-file-upload',

  props: {
    imageClass: {
      type: String,
      default: ''
    },
    inputClass: {
      type: String,
      default: ''
    },
    accept: {
      type: String,
      default: 'image/png,image/gif,image/jpeg'
    },
    maxSize: {
      type: Number,
      default: 10 // megabytes
    },
    disablePreview: {
      type: Boolean,
      default: false
    },
    fileName: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: 'Click here to upload image'
    },
    defaultPreview: {
      type: String,
      default: ''
    }
  },

  data () {
    return {
      file: null,
      files: [],
      preview: null,
      visiblePreview: false,
      previewImages: []
    }
  },

  computed: {
    wrapperStyles () {
      return {
        'position': 'relative',
        'width': '100%'
      }
    },

    fileInputStyles () {
      return {
        'width': '100%',
        'position': 'absolute',
        'top': 0,
        'left': 0,
        'right': 0,
        'bottom': 0,
        'opacity': 0,
        'overflow': 'hidden',
        'outline': 'none',
        'cursor': 'pointer'
      }
    },

    textInputStyles () {
      return {
        'width': '100%',
        'cursor': 'pointer'
      }
    },

    previewImage () {
      return this.preview || this.defaultPreview
    }
  },

  methods: {
    removeOrderAttachment (index) {
      this.$emit('removeOrderAttachment', index)
      this.files.splice(index, 1)
      this.previewImages.splice(index, 1)
    },
    onChange: function onChange (e) {
      let _this = this

      let files = e.target.files || e.dataTransfer.files
      if (!files.length) {
        return
      }

      function readAndPreview (file) {
        let reader = new FileReader()
        let size = file.size && file.size / Math.pow(1000, 2)

        // check file max size
        if (size > _this.maxSize) {
          _this.$emit('size-exceeded', size)
          return
        }

        reader.onload = function (e) {
          // update file
          _this.$emit('file', file)

          let dataURI = e.target.result

          if (dataURI) {
            _this.$emit('load', dataURI)

            // _this.preview = dataURI;
            _this.previewImages.push(dataURI)
            _this.files.push(file)
          }
        }

        // read blob url from file data
        reader.readAsDataURL(file)
      }

      if (files) {
        [].forEach.call(files, readAndPreview)
      }
    }
  },

  template: `
    <div class="vue-base64-file-upload">
      <div class="vue-base-64-img-container">
        <img
          v-for="previewImg in previewImages"
          v-show="!disablePreview"
          :src="previewImg"
          :class="imageClass" />
      </div>
      <div class="vue-base64-file-upload-wrapper" :style="wrapperStyles">
        <input
          type="file"
          multiple
          @change="onChange"
          :style="fileInputStyles"
          :accept=accept />
        <input
          type="text"
          :class="inputClass"
          :style="textInputStyles"
          :value="fileName || file && file.name"
          :placeholder="placeholder"
          disabled />
      </div>
      <ul style="margin-top: 10px">
        <li class="d-flex" v-for="(file, index) in files" :key="file+index">
          <b-button @click="removeOrderAttachment(index)" class="mr-2 mb-2 text-white" size="sm" variant="danger">
            <span class="fa fa-remove fa-lg"></span>
          </b-button>
          {{ file.name }}
        </li>
      </ul>
    </div>
  `
}
