export default {
  items: [
    {
      name: 'Strona główna',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Zamówienia',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Zamówienia',
      url: '/orders',
      icon: 'icon-basket',
      children: [
        {
          name: 'Lista zamówień',
          url: '/offers',
          icon: 'icon-list'
        },
        {
          name: 'Dodaj zamówienie',
          url: '/new-offer',
          icon: 'icon-plus'
        }
      ]
    },
    {
      title: true,
      name: 'Szablony',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Szablony',
      url: '/templates',
      icon: 'icon-equalizer',
      children: [
        {
          name: 'Lista szablonów',
          url: '/templates',
          icon: 'icon-list'
        },
        {
          name: 'Nowy szablon',
          url: '/form-configurator',
          icon: 'icon-plus'
        }
      ]
    },
    {
      name: 'Parametry',
      url: '/parameters',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Lista parametrów',
          url: '/parameters',
          icon: 'icon-list'
        },
        {
          name: 'Nowy parametr',
          url: '/param-configurator',
          icon: 'icon-plus'
        }
      ]
    },
    // {
    //   title: true,
    //   name: 'Produkty',
    //   class: '',
    //   wrapper: {
    //     element: '',
    //     attributes: {}
    //   }
    // },
    // {
    //   name: 'Produkty',
    //   url: '/products',
    //   icon: 'icon-bag',
    //   children: [
    //     {
    //       name: 'Lista produtków',
    //       url: '/products/list',
    //       icon: 'icon-list'
    //     },
    //     {
    //       name: 'Nowy produkt',
    //       url: '/products-configurator',
    //       icon: 'icon-plus'
    //     }
    //   ]
    // },
    {
      title: true,
      name: 'Klienci',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Klienci',
      url: '/clients',
      icon: 'icon-people',
      children: [
        {
          name: 'Lista klientów',
          url: '/clients/list',
          icon: 'icon-list'
        },
        {
          name: 'Dodaj klienta',
          url: '/clients/add',
          icon: 'icon-user-follow'
        }
      ]
    },
    {
      title: true,
      name: 'Zarządzanie',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'DK',
      url: '/departments',
      icon: 'icon-settings',
      forAdmin: true,
      children: [
        {
          name: 'Lista działów',
          url: '/departments/list',
          icon: 'icon-list'
        },
        {
          name: 'Dodaj dział',
          url: '/departments/add',
          icon: 'icon-user-follow'
        }
      ]
    }
  ]
}
