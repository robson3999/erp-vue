import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Dashboard from '@/views/Dashboard'

Vue.use(BootstrapVue)

describe('Dashboard', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Dashboard)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('#Dashboard').textContent).to.equal('Dashboard')
  })
})
