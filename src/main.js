// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './polyfill'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router/index'
import VeeValidate, { Validator } from 'vee-validate'
import pl from 'vee-validate/dist/locale/pl'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import { options } from '@/shared/toasts'
Validator.localize('pl', pl)

Vue.use(BootstrapVue)
Vue.use(VeeValidate, {fieldsBagName: 'formFields'})

miniToastr.init()

Vue.use(VueNotifications, options)

// remove when deploying to prod
Vue.config.devtools = true

const store = {
  debug: false,
  state: {
    userRole: ''
  },
  setUserRole (newVal) {
    if (this.debug) console.log('setUserRole triggered with ', newVal)
    this.state.userRole = newVal
  },
  getUserRole () {
    if (this.debug) console.log(this.state.userRole)
    return this.state.userRole
  },
  clearUserRole () {
    if (this.debug) console.log('clearUserRole triggered')
    this.state.userRole = ''
  }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data: {
    store
  },
  template: '<App/>',
  components: {
    App
  }
})
