import miniToastr from 'mini-toastr'// https://github.com/se-panfilov/mini-toastr

export const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

export const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}
