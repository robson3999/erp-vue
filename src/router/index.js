import Vue from 'vue'
import Router from 'vue-router'
import { isLoggedIn } from './auth'
// Containers
import DefaultContainer from '@/containers/DefaultContainer'

// General
import Dashboard from '@/views/Dashboard'
// import Page404 from '@/views/static_pages/Page404'

// Offers
import AddOffer from '@/views/offers/v2/AddOffer'
import OffersList from '@/views/offers/v2/OffersList'
import Table from '@/views/offers/v2/offersList/Table'
import ViewOffer from '@/views/offers/v2/ViewOffer'

// Clients
import ClientProfile from '@/views/clients/ClientProfile'
import ClientsList from '@/views/clients/ClientsList'
import ClientAdd from '@/views/clients/ClientAdd'
import ClientEdit from '@/views/clients/ClientEdit'
import ClientAddressNew from '@/views/clients/adding/ClientAddressNew'
import ClientNew from '@/views/clients/adding/ClientNew'

// Templates
import FormConfigurator from '@/views/templateConfigurator/FormConfiguratorV2'
import FormGenerator from '@/views/templateConfigurator/FormGenerator'
// import FormEditor from '@/views/templateConfigurator/FormEditor'
import FormList from '@/views/templateConfigurator/FormList'

// Parameters
import ParamConfigurator from '@/views/parametersConfigurator/ParamConfigurator'
import ParamGenerator from '@/views/parametersConfigurator/ParamGenerator'
import ParamList from '@/views/parametersConfigurator/ParamList'

// Products
import productsList from '@/views/productsConfigurator/productsList'
import productGenerator from '@/views/productsConfigurator/productGenerator'
import productsConfigurator from '@/views/productsConfigurator/productsConfigurator'

// Departments
import DepartmentsList from '@/views/departments/DepartmentsList'
import DepartmentsAdd from '@/views/departments/DepartmentAdd'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      meta: { requiresAuth: true },
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: '/new-offer',
          name: 'Dodawanie oferty',
          component: AddOffer
        },
        {
          path: '/offers',
          component: OffersList,
          children: [
            {
              path: '',
              name: 'Lista zamówień',
              component: Table
            },
            {
              path: '/offers/:id',
              name: 'Wyświetlanie zamówienia',
              component: ViewOffer
            }
          ]
        },
        {
          path: 'templates',
          redirect: '/templates/list',
          name: 'Formularze',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'Lista',
              component: FormList
            },
            {
              path: ':id',
              name: 'Formularz szablonu',
              component: FormGenerator
            },
            {
              path: ':id/edit',
              name: 'Edycja szablonu',
              component: FormConfigurator
            }
          ]
        },
        {
          path: 'parameters',
          redirect: '/parameters/list',
          name: 'Parametry',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'Lista',
              component: ParamList
            },
            {
              path: ':id',
              name: 'Formularz szablonu',
              component: ParamGenerator
            },
            {
              path: ':id/edit',
              name: 'Edycja szablonu',
              component: ParamConfigurator
            }
          ]
        },
        {
          path: 'products',
          redirect: '/products/list',
          name: 'Produkty',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'Lista produktów',
              component: productsList
            },
            {
              path: ':id',
              name: 'Formularz produktu',
              component: productGenerator
            },
            {
              path: ':id/edit',
              name: 'Edycja produktu',
              component: productsConfigurator
            }
          ]
        },
        {
          path: '/products-configurator',
          name: 'Konfigurator produktów',
          component: productsConfigurator
        },
        {
          path: '/form-configurator',
          name: 'Konfigurator szablonów',
          component: FormConfigurator
        },
        {
          path: '/param-configurator',
          name: 'Konfigurator parametrów',
          component: ParamConfigurator
        },
        {
          path: 'clients',
          redirect: '/clients/list',
          name: 'Klienci',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'Lista klientów',
              component: ClientsList
            },
            {
              name: 'Dodaj klienta',
              path: 'add',
              component: ClientAdd
            },
            {
              path: ':id',
              name: 'Profil klienta',
              component: ClientProfile
            },
            {
              path: ':id/edit/*',
              name: 'Edycja klienta',
              component: ClientEdit
            },
            {
              path: ':id/addresses',
              name: 'Dodawanie adresów',
              component: ClientAddressNew
            },
            {
              path: ':id/clients',
              name: 'Dodawanie klientów',
              component: ClientNew
            }
          ]
        },
        {
          path: 'departments',
          redirect: '/departments/list',
          name: 'Działy konstrukcyjne',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'Lista działów konstrukcyjnych',
              component: DepartmentsList
            },
            {
              path: 'add',
              name: 'Dodawanie działu',
              component: DepartmentsAdd
            }
          ]
        }
      ]
    },
    {
      path: '/index',
      redirect: '/'
    }
    // { path: '*', component: Page404 }
  ]
})

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let isLoggedInStatus = await isLoggedIn()
    if (!isLoggedInStatus) {
      window.location.replace('/')
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
